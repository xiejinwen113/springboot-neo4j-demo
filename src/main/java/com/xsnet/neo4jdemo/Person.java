package com.xsnet.neo4jdemo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * @program: neo4j-demo
 * @description: person
 * @author: Jamie.shi
 * @create: 2020-05-27 10:29
 **/
@NodeEntity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    private String  name;
    private Integer age;
}
