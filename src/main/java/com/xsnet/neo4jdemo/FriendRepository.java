package com.xsnet.neo4jdemo;

import org.springframework.data.neo4j.repository.Neo4jRepository;

/**
 * @program: neo4j-demo
 * @description: 朋友关系
 * @author: Jamie.shi
 * @create: 2020-05-27 11:28
 **/
public interface FriendRepository  extends Neo4jRepository<Friend, Long> {
}
