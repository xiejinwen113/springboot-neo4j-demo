package com.xsnet.neo4jdemo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class Neo4jDemoApplicationTests {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private FriendRepository friendRepository;

    @Test
    void contextLoads() {
    }

    /**
     * 添加节点
     */
    @Test
    public void addNodeTest(){
        Person person=new Person();
        person.setName("jamie");
        person.setAge(18);
        personRepository.save(person);
    }
    /**
     * 删除节点
     */
    @Test
    public void deleteNodeTest(){
        personRepository.deleteById(3L);
    }

    /**
     * 修改节点
     */
    @Test
    public void updateNodeTest(){
        Person person=new Person();
        person.setName("jamie");
        person.setAge(19);
        person.setId(3L);
        personRepository.save(person);
    }

    /**
     * 添加朋友关系
     */
    @Test
    public void addNodeRSTest(){
        Person person=new Person();
        person.setName("rose3");
        person.setAge(18);
        Person rose = personRepository.save(person);

        Person jamie = personRepository.findByName("jamie");

        Friend friend=new Friend(rose,jamie,"附近的人");

        friendRepository.save(friend);
    }

    /**
     * 查找某人所有朋友
     */
    @Test
    public void findFriendByPersonTest(){
        List<Person> friends = personRepository.findFriendByPerson("jamie");
        friends.forEach(System.out::println);
    }

}
